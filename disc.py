#!/usr/bin/python3
# Discord Submodule (pip3 install discord yarl==1.4.2)
# Documentation: https://realpython.com/how-to-make-a-discord-bot-python/

import json, discord, threading, traceback
from discord.ext import tasks
from copy import copy
from utils import date_as_sql, DISCORDAPI

# TODO: https://discordpy.readthedocs.io/en/latest/api.html#discord.Webhook.send
# Fetch token here: https://discordapp.com/developers/applications/
f=open("pass.json", "r")
secret=json.load(f)
f.close()

channel=str(secret["disc_channel"])
master=str(secret["disc_master"])
token=str(secret["disc_token"])

# TODO: https://docs.microsoft.com/en-us/graph/teams-proactive-messaging
if secret["use_teams"]:
    import mail
    MAILER=True
else:
    MAILER=False

MEMORY=[]
BUFFER=[]
UPSTREAM=None
MYSTREAM=None

# Stage 0: Setup the client
print("\033[1mStarting Discord bot...\033[0m")
client = discord.Client()
#ttl=Translator()

@client.event
async def on_ready():
    global channel
    #print(f'{client.user} has connected to Discord!')
    print('%s has connected to Discord!' % client.user)

    for guild in client.guilds:
        #print(f"We're in {guild.name} (id: {guild.id})")
        print("We're in %s (id: %d)" % (guild.name, guild.id))
        #await message.channel.send(response)

    wait_and_message.start()
    print("\033[1mDone!\033[0m")
    #exit(0)

@client.event
async def on_message(message):
    global BUFFER, DISCORDAPI
    if message.author == client.user:
        return

    mes=copy(message.content)
    if message.content.startswith("!"):
        message.content=""
    else:
        # Log and deliver translation
        print("DC.Send: \"%s\"" % (mes))
        BUFFER.append([DISCORDAPI,
                       mes,
                       "%s | Discord" % (message.author)])

    # Message sent to the master channel
    if MAILER and message.channel.name.replace("#", "") == master:
        print("Message belongs to super channel, forwarding")
        m = threading.Thread(target=mail.send, args=(mes, message.author,))
        m.daemon = True
        m.start()
    #await message.channel.send(response)



@tasks.loop(seconds=1.0)
async def wait_and_message():
    global channel, client, MEMORY, UPSTREAM, BUFFER, DISCORDAPI
    for msg in MEMORY:
        if msg[0] != DISCORDAPI:
            print("DC/RECEIVED: %s" % msg)
            for guild in client.guilds:
                chans = discord.utils.get(guild.text_channels, name=channel)
                print("DC/SEND TO %s" % channel)
                await chans.send("(%s) %s: %s" % (date_as_sql(), msg[2], msg[1]))
        MEMORY.remove(msg)

    for msg in BUFFER:
        print("DC/LOOPBACK MESSAGE: %s" % msg)
        UPSTREAM.put(msg)
        BUFFER.remove(msg)



def listener():
    global MYSTREAM
    while True:
        obj = MYSTREAM.get()
        print("Discord: Received %s" % str(obj))
        try:
            if obj[0] == DISCORDAPI:
                pass
            MEMORY.append(obj)
        except:
            traceback.print_exc()
    return

def do_thread(q,s):
    global UPSTREAM, MYSTREAM
    print("Starting queue listener")
    UPSTREAM = q
    MYSTREAM = s
    t = threading.Thread(target=listener)
    t.daemon = True
    t.start()
    print("Firing up Discord")
    client.run(token)
    print("\033[31;1mDiscord: TERMINATED\033[0m")



