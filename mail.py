########################################################################################
#     This file is part of Moubootaur Legends API.
#     Copyright (C) 2020  Jesusalva

#     This library is free software; you can redistribute it and/or
#     modify it under the terms of the GNU Lesser General Public
#     License as published by the Free Software Foundation; either
#     version 2.1 of the License, or (at your option) any later version.

#     This library is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#     Lesser General Public License for more details.

#     You should have received a copy of the GNU Lesser General Public
#     License along with this library; if not, write to the Free Software
#     Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
########################################################################################
# Sends email

import json, smtplib, traceback
from copy import copy
from utils import stdout, ifte
f=open("pass.json", "r")
p=json.load(f)
f.close()

SMTP_ONLINE=p["use_teams"]
SMTP_SERVER=p["smtp_server"]
SMTP_PORT=p["smtp_port"]
SMTP_USER=p["smtp_user"]
SMTP_PASS=p["smtp_pass"]
SMTP_SEND=p["smtp_send"]

def header(sender, target, subject="", html=False, utf=True):
    if html:
        t="html"
    else:
        t="plain"

    if utf:
        u="UTF-8"
    else:
        u="us-ascii"

    if subject != "":
        s=subject
    else:
        s="(No subject)"

    m='Content-Type: text/%s; charset="%s"\nMIME-Version: 1.0\nContent-Transfer-Encoding: 7bit\nSubject: %s\nFrom: %s\n%s\n' % (t, u, s, sender, ifte(False, "To: %s\n" % target, ""))
    return m

def send(message, subject=""):
    if not SMTP_ONLINE:
        return
    user=copy(SMTP_USER)
    password=copy(SMTP_PASS)
    target=copy(SMTP_SEND)
    fromaddr="%s\r\n" % user
    print(str(fromaddr))
    target+="\r\n"
    #msg = ("From: %s\r\nTo: %s\r\n\r\n" % (fromaddr, target))
    msg = header(fromaddr, target, subject)
    msg+=str(message.replace('\n', '\r\n'))
    msg+="\r\n"
    msg=msg.encode("utf8")
    #msg=message

    print("Message length is " + repr(len(msg)))

    try:
        server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        server.set_debuglevel(1)
        server.starttls()
        server.login(user, password)
        print("Authentication complete!")
        fail=server.sendmail(fromaddr, target, msg)
        print("Failed to deliver: "+str(fail))
        server.quit()
        stdout("MAIL: Sent with success!\n\nMAIL: SUCCESS")
    except smtplib.SMTPRecipientsRefused:
        traceback.print_exc()
        stdout("FATAL: No message was delivered")
        return False
    except:
        traceback.print_exc()
        stdout("MAIL: An error happened, try again later.")
        return False
    return True

#print("// End")

