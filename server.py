#!/usr/bin/python3

## Global
import json, syslog, time, traceback, threading
from multiprocessing import Process, Queue
LOG_AUTH=syslog.LOG_AUTH

## Local
from utils import now, stdout, debug, SERVERAPI

# SECRET goes here
f=open("pass.json", "r")
p=json.load(f)
f.close()
zapon=p["use_zapzap"]

memory = Queue()
servmem = Queue()
discmem = Queue()
telemem = Queue()
zapmem = Queue()

print("Preparing to start")
## Modular
import disc, teleg
discproc = Process(target=disc.do_thread, args=(memory,discmem,))
teleproc = Process(target=teleg.do_thread, args=(memory,telemem,))
if zapon:
    import zapzap
    zapproc = Process(target=zapzap.do_thread, args=(memory,zapmem,))
else:
    zapproc = None

def InitDiscord():
    print("Starting Discord in parallel")
    discproc.start()

def InitTelegram():
    print("Starting Telegram in parallel")
    teleproc.start()

def InitZapZap():
    if not zapon:
        return
    print("Starting WhatsApp in parallel")
    zapproc.start()

def MemoryHandler():
    global memory, servmem, discmem, telemem, zapmem, zapon
    while True:
        obj = memory.get()
        print("Server: Received %s" % str(obj))
        #servmem.put(obj)
        discmem.put(obj)
        telemem.put(obj)
        if zapon:
            zapmem.put(obj)

###############################################################
# Begin stuff
stdout("Starting at: T-%d" % (now()))
syslog.openlog()
InitDiscord()
InitTelegram()
InitZapZap()
time.sleep(1.5)

try:
    print("Now starting memory handler")
    t = threading.Thread(target=MemoryHandler)
    t.daemon = True
    t.start()
    while True:
        command=str(input("> "))
        # No command inserted? Do nothing
        if command == "":
            continue
        # We have a command, prepare it for processing
        stdout("CONSOLE: %s" % command)
        cmd=command.split(' ')[0].lower()
        com=" ".join(command.split(' ')[1:])
        # Parse the command
        if cmd in ["exit", "quit", "close", "term", "end"]:
            stdout("Preparing to close server...")
            break
        elif cmd in ["dbg", "debug"]:
            debug = not debug
            stdout("Changed debug mode to %s" % str(debug))
        elif cmd in ["raw", "eval"] and debug:
            try:
                print(eval(com))
            except:
                traceback.print_exc()
                print("[RAW] Error.")
        elif cmd in ["run", "exec"] and debug:
            try:
                exec(com)
            except:
                traceback.print_exc()
                print("[RUN] Error.")
        elif cmd in ["send"]:
            memory.put([SERVERAPI, com, "Server"])
        else:
            stdout("ERROR: Unknown command.")
except:
    stdout("Abrupt error: Terminating!")
    traceback.print_exc()

# Wait a bit before disconnecting all clients
# To make sure any pending operation will arrive
time.sleep(0.20)
discproc.terminate()
teleproc.terminate()
if zapon:
    zapproc.terminate()

print("Server finished.")


