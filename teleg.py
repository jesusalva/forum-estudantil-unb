#!/usr/bin/python3
# Telegram Submodule (python-telegram-bot)
# See also: https://stackoverflow.com/questions/32423837/telegram-bot-how-to-get-a-group-chat-id

import json, time, threading, traceback, requests
from telegram import Update
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, CallbackContext
from utils import date_as_sql, TELEGRAMAPI
import logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                     level=logging.ERROR)

# SECRET goes here
f=open("pass.json", "r")
p=json.load(f)
f.close()

TOKEN=p["tele_token"]
CHAN=p["tele_channel"]

MEMORY=[]
BUFFER=[]
UPSTREAM=None
MYSTREAM=None


# /start command
def start(update: Update, context: CallbackContext) -> None:
    #ctx.bot.send_message(chat_id=update.effective_chat.id, text="Beep boop")
    update.message.reply_text("Beep boop")

def echo(update: Update, context: CallbackContext) -> None:
    global CHAN
    if str(update.effective_chat.id) != str(CHAN):
        update.message.reply_text(update.message.text)
        """
        print("Message not in channel: %s\n\
               Sent by %s %s" % (str(update.effective_chat.id),
               update.effective_user.first_name, update.effective_user.last_name or ""))
        """
    else:
        BUFFER.append([TELEGRAMAPI,
                       update.message.text,
                       "%s %s | Telegram" % (
                           update.effective_user.first_name,
                           update.effective_user.last_name or "")])

def sendraw(msg):
    global CHAN, TOKEN
    payload = {
        "chat_id": CHAN,
        "text": msg}
    url = "https://api.telegram.org/bot%s/sendMessage" % (TOKEN)
    r = requests.post(url, data=payload)
    #print(r.text)
    try:
        print("Telegram: %d" % int(r.status_code))
    except:
        pass
    return

"""
def alarm(context):
    job = context.job
    context.bot.send_message(job.context, text='Beep!')

def set_timer(update: Update, context: CallbackContext) -> None:
    chat_id = CHAN
    due = 0
    context.job_queue.run_once(alarm, due, context=chat_id, name=str(chat_id))
"""

bot = Updater(token=TOKEN, use_context=True)
evt = bot.dispatcher
evt.add_handler(CommandHandler("start", start))
evt.add_handler(MessageHandler(Filters.text & ~Filters.command, echo))

######
def wait_and_message():
    global CHAN, MEMORY, UPSTREAM, BUFFER, TELEGRAMAPI, bot
    time.sleep(0.5)
    print("Telegram Bot is now running")
    while True:
        for msg in MEMORY:
            if msg[0] != TELEGRAMAPI:
                print("TL/RECEIVED: %s" % msg)
                print("TL/SEND TO %s" % CHAN)
                try:
                    sendraw("(%s) %s: %s" % (date_as_sql(), msg[2], msg[1]))
                except:
                    traceback.print_exc()
            MEMORY.remove(msg)

        for msg in BUFFER:
            print("TL/LOOPBACK MESSAGE: %s" % msg)
            UPSTREAM.put(msg)
            BUFFER.remove(msg)
        time.sleep(1.0)


def listener():
    global MYSTREAM
    while True:
        obj = MYSTREAM.get()
        print("Telegram: Received %s" % str(obj))
        try:
            if obj[0] == TELEGRAMAPI:
                pass
            MEMORY.append(obj)
        except:
            traceback.print_exc()
    return

def do_thread(q, s):
    global UPSTREAM, MYSTREAM
    print("Starting queue listener")
    UPSTREAM = q
    MYSTREAM = s
    t = threading.Thread(target=listener)
    t.daemon = True
    t.start()
    T = threading.Thread(target=wait_and_message)
    T.daemon = True
    T.start()
    print("Firing up Telegram")
    bot.start_polling()
    bot.idle()
    print("\033[31;1mTelegram: TERMINATED\033[0m")


