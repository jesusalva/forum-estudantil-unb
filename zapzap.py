#!/usr/bin/python3
# WhatsApp Submodule (webwhatsapi - included w/o dependencies)
# `pip3 install -r webwhatsapi/requeriments.txt`
# `apt-get install firefox-geckodriver`
# Documentation: https://github.com/mukulhase/WebWhatsapp-Wrapper

import json, time, threading, traceback, os
from webwhatsapi import WhatsAPIDriver
from webwhatsapi.objects.message import Message
from utils import stdout, date_as_sql, WHATSAPPAPI
driver = None

# SECRET goes here
f=open("pass.json", "r")
p=json.load(f)
f.close()

ZAPON=p["use_zapzap"]
ZAPID=p["zapzap_id"]

MEMORY=[]
BUFFER=[]
UPSTREAM=None
MYSTREAM=None


######
def sendraw(msg):
    global driver, ZAPID
    try:
        driver.send_message_to_id(ZAPID, msg)
    except:
        traceback.print_exc()
    return

def wait_and_message():
    global CHAN, MEMORY, UPSTREAM, BUFFER, WHATSAPPAPI, bot
    time.sleep(0.5)
    print("WhatsApp Bot is now running")
    while True:
        for msg in MEMORY:
            if msg[0] != WHATSAPPAPI:
                print("ZP/RECEIVED: %s" % msg)
                try:
                    sendraw("(%s) %s: %s" % (date_as_sql(), msg[2], msg[1]))
                except:
                    traceback.print_exc()
            MEMORY.remove(msg)

        for msg in BUFFER:
            print("ZP/LOOPBACK MESSAGE: %s" % msg)
            UPSTREAM.put(msg)
            BUFFER.remove(msg)
        time.sleep(1.0)


def listener():
    global MYSTREAM
    while True:
        obj = MYSTREAM.get()
        print("WhatsApp: Received %s" % str(obj))
        try:
            if obj[0] == WHATSAPPAPI:
                pass
            MEMORY.append(obj)
        except:
            traceback.print_exc()
    return

def do_thread(q, s):
    global UPSTREAM, MYSTREAM, driver
    if not ZAPON:
        return
    print("Starting WhatsApp Driver...")
    profp = os.path.dirname(os.path.realpath(__file__))+"/firefox.profile/"
    print("Loading profile %s" % (profp))
    driver = WhatsAPIDriver(profile=profp)
    print("ZP: Waiting for QR")
    ready = False
    try:
        driver.wait_for_login()
        ready = True
    except:
        traceback.print_exc()
        pass

    while not ready:
        stdout("\033[1mWhatsApp login failed (timeout/low battery), retry in 5s\033[0m")
        time.sleep(5.0)
        try:
            driver.wait_for_login()
            ready = True
            break
        except:
            traceback.print_exc()
            pass

    print("Starting queue listener")
    UPSTREAM = q
    MYSTREAM = s
    t = threading.Thread(target=listener)
    t.daemon = True
    t.start()
    T = threading.Thread(target=wait_and_message)
    T.daemon = True
    T.start()
    print("Firing up WhatsApp")

    while True:
        time.sleep(1.0)
        for contact in driver.get_unread():
            if contact.chat.id != ZAPID:
                print("ZP: Bad contact")
                continue
            for message in contact.messages:
                if isinstance(message, Message):  # Currently works for text messages only.
                    print("Message found! Relaying...")
                    try:
                     if message.type != "chat":
                        stdout("Unrecognized type: %s" % str(message.type))
                        continue
                     # Send it to the relay
                     BUFFER.append([WHATSAPPAPI,
                                   message.content,
                                   "%s | WhatsApp" % (
                                       message.sender.push_name or "")])
                    except:
                     traceback.print_exc()


    print("\033[31;1mWhatsApp: TERMINATED\033[0m")


